package ru.t1.zvero.tm.api;

import ru.t1.zvero.tm.enumerated.Status;
import ru.t1.zvero.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project create(String name, String description);

    Project create(String name);

    Project add(Project project);

    List<Project> findAll();

    void clear();

    void remove(Project project);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}